extensions [profiler matrix]

globals [
  
;SLIDER VARIABLES;
  ;background-risk                ; the risk that an agent is exposed by virtue of being in the camp.  Default value = 0.1
  ;outside-risk-multiplier        ; the percentage by which latrine risk is adjusted to give the general outside risk
  ;max-daily-risk                 ; the maximum value of risk an agent could theoretically have on any day; used to scale the current-risk
  ;latent-period                  ; default value = 3 days or 18 ticks (range 12-96 hrs on average, but can be up to 1 week for S. dysenteriae)
  ;infectious-period              ; default value = 7 days or 42 ticks (best estimate, literature indicates can be up to one month)
  ;carrier-prob                   ; the probability that an agent becomes an asymptomatic carrier rather than recovering.  Estimates are from 2-5%.  default value = 3.5%
  ;transmission-prob              
  ;death-prob                     ; the probability that an infected agent will die during the current tick
  ;food-energy                    ; allows for the adjustment of energy replenishment during do-Evening-acts
  ;run-length                     ; the maximum number of ticks the model will run before automatically stopping
  
;POPULATION;
  init-pop-size                   ; This variable is used for data recording purposes only and is set equal to the count of agents at the start of a simulation
  prison-pop                      ; The population of the prison at any given time
  num-susceptible                      ; the number of susceptible agents in the population
  num-exposed                          ; the number of exposed agents in the population
  num-infectious                       ; the number of infectious agents in the population
  num-carrier                          ; the number of asymptomatic carriers in the population
  num-recovered                        ; the number of recovered agents in the population
  num-dis-dead                         ; the number of agents which have died from disease
  num-starved                          ; the number of agents who have starved (death from non-diesease causes)
  num-dead                             ; the total number of dead agents (num-dis-dead + num-starved)
 
  RCD                             ; these are values used for keeping tallies for data-recording. RD = recovered + carriers + dead (size of epidemic),
  first-case                      ; the agent-id of the first infected case
  first-case-barracks             ; the barracks/tent id of the first case
  first-case-regiment             ; the regiment of the first infected case
  
;RISK GLOBALS;
  
  latrine-risk                    ; the number of bacteria/risk points agents may gain when they use the latrine.  Correlated with the number of infectious agents in the camp at the time
  outside-risk                    ; the number of bacteria/risk points agents may gain when they go outside, this is a percentage of the latrine-risk.  Correlated with the number of infectious agents 
                                  ; in the camp at the time
  
  ;risk-array                     ; a matrix template that can be used to separate out barracks to 
  barracks-food-array             ; a matrix that holds the values of foodborne infection risk for each barracks
  
;BUILDING INFORMATION;
  building-list
  num-barracks num-hospitals


  
;OTHER;
  timekeeper                      ; determines the simulation's current 4-hour time block so agents call the appropriate activity methods, include this information in print statements, etc. Timekeeper values of
                                  ; 1-6 correspond to blocks on Mon-Fri(6a-10a, 10a-2p, 2p-6p, 6p-10p, 10p-2a, 2a-6a), 7-12 correspond to the same time intervals on Saturdays, 13-18 correspond to Sundays.
;GLOBAL MOVEMENT;
  gang-size
  gang-global
  ration-getters-chosen?          ; True when ration-pickers have already been chosen for a given Sunday.
  
  
;NEW PRISONERS;
  prob-influx                     ; The probability that a new group of prisoners enters the camp, estimated by the proportion of days during the time period being modeled, that data indicate new prisoners 
                                  ; entered the camp.  Tentatively initialized at 0.1.
  num-new-reg-leaders               ; The number of new prisoners entering on an 'influx' day.
  max-agent-id                    ; The highest agent-id number at the time new prisoners begin entering the camp.
  max-regiment-number             ; The highest regiment numer at the time new prisoners begin entering the camp.

  ]

turtles-own [ ; AGENT FEATURES are read in from a .txt file.  See agentinfo.txt for further explanation.
  
;AGENT FEATURES;
  agent-id 
  barracks                        ; the building-id of the barracks to which the agent is assigned, and to which the agent returns at night.
  disease-status                  ; determines whether an agent is susceptible (0), exposed (1), infected (2), asymptomatic carrier (3), recovered (4), or dead (5)
  regiment                        ; the regiment to which the agent belongs. Please see agentinfo.txt
  age  
  energy                          ; the amount of energy an agent has remaining. Energy is initialized at ___ for all agents upon entering the simulation.  Energy is used to undertake various activities.
                                  ; Outdoor movement takes more energy than remaining in the barracks.  Agents are estimated to use 15-20 energy each day, and will regain 9-10 energy each day during the 6:00pm
                                  ; timestep, reflecting the ~900-1000 calories prisoners ate each day, on average.
  starved?                        ; boolean that becomes true only if an agent starves to death
  time-in-camp                    ; The number of ticks that an agent has existed in the camp.  Will vary depending on when agent is created and when they die.
                 
;MOVEMENT TOGGLES;
  present-location                ; The building-id of the patch where an agent is currently located.
  destx                           ; The x coordinate of a patch to which an agent wants to move ("destination")
  desty                           ; The y coordinate of a patch to which an agent wants to move ("destination")
  ration-getter?                  ; boolean variable determining whether the agent is one of two in the barracks to pick up and prepare rations every week.
  needs-group?                    ; boolean variable determining whether an agent is a member of a movement group
  group-leader?                   ; boolean, distinguishing a turtle as the leader of a travel-group
  
;FOOD RELATED
  food-risk-checked?              ; boolean, works as a toggle to ensure that only one agent per barracks calls the method to update his barracks' food risk

  

;INFECTION VARIABLES;

  time-to-infectious              ; Timer variable set equivalent to the latent period that starts running at the tick a agent is infected
  time-to-recovery                ; Timer variable set equivalent to the infectious period that starts running at the tick an infected agent becomes infectious
  time-infected                   ; Tick at which the agent is infected
  place-infected                  ; ID of location where an agent is infected 
  time-died
  place-died
  
  newly-infected?
  newly-dead?
  cumulative-risk                 ; Corresponds to an agent's overall disease risk as a consequence of exposures they have had while in the camp (both background and daily), measured in terms of numbers of
                                  ; bacteria/risk points.  The probability of disease at a particular time is specified by the number of bacteria/risk points in an agent's cumulative risk.  All agents are initialized
                                  ; at the user-specified background risk, a slider variable.
  current-risk                    ; Corresponds to an agent's level of risk at any time during a particular day. Reset each tick. Dependent on an agent's actions regardless of background risk. This variable also takes into 
                                  ; account risk due to contact with contaminated water/food over and above any background level.
  adj-current-risk                ; The ratio of actual current risk to the max-daily-risk. Calculated at the end of each day (timekeeper = 6 or timekeeper = 12)
  barracks-risk                   ; The number of bacteria/risk points agents may gain when in their barracks.  Correlated with number of infectious agents in the barracks at the time.
  gang-risk                       ; The number of bacteria/risk points agents may gain when associating outside with infectious group members.
  
;OTHER;
  step-completed?                 ; determines whether requirements have been met for the model to advance to the next tick
  gang-id                         ; Unique identifier for agents moving in a group
  new-regiment-leader?            ; boolean variable, determines whether a newly-spawned agent is the leader of his regiment group (applies only during the tick when agents are introduced into population)
  bathroom-tick                   ; the tick number of the agent's last visit to the latrine
  ;]

 
]

patches-own [
  occupied?                      ; boolean variable, whether or not an agent is currently located on/occupying a patch
  building-id                    
  building-type
  outside?                       ; boolean variable, indicates whether a patch is an "outside" patch or a "building" patch
  ration-area?                   ; boolean variable, indicates whether agents fetching weekly rations should report to that patch
  ration-area-patches            ; the set of patches where agents fetching rations report to receive rations
  ]

breed [agents agent]
breed [ghosts ghost]



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;       SETUP METHODS       ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;------------------------------------------------------------------------

to profile
  
  setup                  ;; set up the model
  profiler:start         ;; start profiling
  repeat 100 [ go ]      ;; run something you want to measur
  profiler:stop          ;; stop profiling
  print profiler:report  ;; view the results
  profiler:reset         ;; clear the data

end

;------------------------------------------------------------------------

to setup                 ; the setup procedure resets map and data from previous runs and begins a new model instance.
  clear-all              ; resets the map and clears all data from previous runs
  ask patches [          ; sets baseline values for all patches, such that they are unoccupied and have no retained building information from previous runs.
    set pcolor 39
    set occupied? false
    set building-id 0]
  import-map-data         
  import-agent-data
  initialize-globals
  ask one-of agents [    ; selects one agent at random to be the first case, makes him infectious, and resets all other infection-related variables for the population to ensure that he is the only infectious
                         ; agent before the model run begins. He also updates files to record individual information on his location and regiment.
    set first-case agent-id
    set disease-status 2 
    set num-infectious 1
    set num-susceptible num-susceptible - 1
    set color red
    set time-infected 0
    set time-to-recovery infectious-period
    set first-case-barracks barracks
    set first-case-regiment regiment
    set place-infected [building-id] of patch-here]

  create-files
  reset-ticks 
end

;------------------------------------------------------------------------


; The map is a 75x75 grid, with each patch representing 9 sq. ft. of space.  There are 6 barracks measuring 4x24 patches which, at capacity, hold 576 individuals, 96 per barrack.  The mean 
; population density for this model is 1 individual per 87 sq. ft, which approximates the real-world conditions at Camp Douglas, where each individual had 8 sq. ft. in barracks, (9 sq. ft.  
; in this model) and roughly 80 sq. ft. total outside.

to import-map-data
  
  set building-list []
  file-open "CDbuildings.txt"              ; this procedure reads the corner coordinates and side-lengths in from an associated file, and provides the patches within the building's footprint with the correct 
                                           ; identifying information.
  while [not file-at-end?] [
    let items read-from-string (word "[" file-read-line "]") 
    
    let llc-x item 0 items
    let llc-y item 1 items
    let building-width item 2 items
    let building-length item 3 items 
    ask patch llc-x llc-y [
      set building-id item 4 items
      set building-type item 5 items

      ; The following lines add the coordinates of each patch of a given building-type to a list, accessible to agents seeking a certain patch type.
      if building-type = 1 [
          set num-barracks num-barracks + 1
          set building-list lput building-id building-list]                 ; Barracks
        if building-type = 2 [set num-hospitals num-hospitals + 1]          ; Medical building
    ] ; close ask patch llc-x llc-y

    ;The following lines of code finds all patches in a building and sets each patch's ID and type to match the values assigned to the lower left corner
    let building-patches (patch-set patches with [pxcor >= llc-x and pxcor < llc-x + building-width and pycor >= llc-y and pycor < llc-y + building-length])
    ask building-patches [ 
      set building-id [building-id] of patch llc-x llc-y
      set building-type [building-type] of patch llc-x llc-y
    ] ; close ask building-patches
    
    ; the following lines of code set the appropriate map colors for the buildings once patches have been associated with individual buildings    
    ask patches [
      ;set occupied? false  
      if building-type = 1 [set pcolor 3]                                   ; Barracks
      if building-type = 2 [set pcolor 55]                                  ; Medical building
      if building-type = 4 [set pcolor 38 set outside? true]                ; Ration area
      ifelse pcolor = 39 [set outside? true set building-type 5 set building-id 500] [if pcolor != 38 [set outside? false]]
      
 
    ] ; close ask patches/set color 
  ] ; close while not file-at-end?
 
      
  file-close
  

end

;------------------------------------------------------------------------

to import-agent-data
  
  
  file-open "CDagents450.txt"
  ; agent-data are read in from a .txt file.  Each line of data contains the values for the first 6 attributes for a single agent in the order listed below . 
  while [not file-at-end?]
  [
    let items read-from-string (word "[" file-read-line "]")              ; Items is a temporary list of variables read in as string but converted to the appropriate variable type. "Word" concatenates 
                                                                          ; the brackets to the line being read in, because list arguments need to be in brackets (see Netlogo user manual)
    create-agents 1 [
      set agent-id item 0 items
      set barracks item 1 items
      set disease-status item 2 items
      set regiment item 3 items
      set age item 4 items
      set energy 1500
      set starved? false
      set time-in-camp 0
      
      set present-location 0
      set bathroom-tick 0
      
      set time-to-infectious latent-period
      set time-to-recovery infectious-period
      set time-infected -1
      set place-infected -1
      set time-died -1
      set place-died -1
      
      set ration-getter? false
      set new-regiment-leader? false
      set current-risk background-risk
      set cumulative-risk background-risk
      set barracks-risk 0
      set food-risk-checked? false
      set bathroom-tick -20
      
      set newly-infected? false
      set newly-dead? false
      set step-completed? false
      
      set shape "circle"
      set color white
      set size .8
      
      set gang-id 0
    ]
  ]  
  file-close
  
  ask turtles [
    initialize-home
  ] 
end


;------------------------------------------------------------------------

to initialize-globals 
  
  ;; this procedure sets baseline values for all global variables.
  set init-pop-size count agents
  set prison-pop count agents
  set num-susceptible count agents 
  set num-infectious 0
  set num-recovered 0
  set num-carrier 0
  set num-dead 0
  set RCD 0
  
  set first-case 0 
  set first-case-barracks 0
  set first-case-regiment 0
 
  set timekeeper 0
  set ration-getters-chosen? false
  set barracks-food-array make-risk-array
  set outside-risk 0
  
  set prob-influx .2                     
  set num-new-reg-leaders 3            
  set max-agent-id 0
  set max-regiment-number 0
  
end

;------------------------------------------------------------------------


to-report make-risk-array
  ; this procedure creates a matrix associated with food-associated transmission.  
  let risk-list[]                              ; creates an empty array
  let risk-index 0
  while [risk-index < num-barracks] [          ; fills each space in the array with baseline values, in this case, 0
      set risk-list lput 0 risk-list
      set risk-index risk-index + 1            ; increments until the whole array has been populated with baseline values
  ]
  let risk-array matrix:from-column-list (list (building-list)(risk-list))
  report risk-array
end

;------------------------------------------------------------------------

to create-files
 ; The following four lines can be called before creating a data file so that if that file has been used before, it gets erased before adding data for a new run. See Railsback & Grimm (2012)
  ; for examples.
 ; if (file-exists? "CasesData.csv")[
  ;  carefully
   ;   [file-delete "CasesData.csv"]
    ;  [print error-message]]
  
  ; The next three blocks of code put headers at the top of new output files (one each for case, daily and final data) and then the files are closed again, which must happen before a 
  ; simulation begins. The process of inserting headers only happens if the files don't exist already. If a file does exist, output data are just appended to data from previous simulations 
  ; without inserting headers again.
  
  
  if (not file-exists? (word "DailyData-" behaviorspace-run-number ".csv"))[
    file-open (word "DailyData-" behaviorspace-run-number ".csv")
    file-type "Run Number,"
    file-type "Tick, " 
    file-type "Initial Population Size, "
    file-type "Current Prison Population, "
    file-type "Background Risk, "
    file-type "Outside Risk Multiplier, "
    file-type "Maximum Daily Risk, "
    file-type "Food Energy, "
    file-type "Latent Period, "
    file-type "Infectious Period, "
    file-type "Carrier Probability, "
    file-type "Mortality Probability, "
    file-type "First Case, " 
    file-type "Susceptible, "
    file-type "Newly Infected, "
    file-type "Exposed, "
    file-type "Infectious, "
    file-type "Carrier, "
    file-type "Recovered, "
    file-type "Newly Dead (Disease), "
    file-type "Newly Dead (Starvation), "
    file-print "Total Dead, "
  file-close]
  
  if (not file-exists? (word "CasesData-" behaviorspace-run-number ".csv")) [
    file-open (word "CasesData-" behaviorspace-run-number ".csv")
    file-type "Run Number," 
    file-type "Tick, " 
    file-type "Population Size, "
    file-type "Current Prison Population, "
    file-type "Background Risk, "
    file-type "Outside Risk Multiplier, "
    file-type "Maximum Daily Risk, "
    file-type "Food Energy, "
    file-type "Latent Period, "
    file-type "Infectious Period, "
    file-type "Carrier Probability, "
    file-type "Mortality Probability, "
    file-type "First Case, "
    file-type "First Case Barracks, "
    file-type "First Case Regiment, " 
    file-type "Agent ID, "
    file-type "Agent Barracks, "
    file-type "Agent Regiment, "
    file-type "Time Infected, "
    file-type "Place Infected, "
    file-type "Disease Status, "
    file-type "Time Died, "
    file-type "Place Died, "
    file-type "Starved?, "
    file-print "Time in Camp, "
  file-close]
  
  if (not file-exists? (word "FinalData-" behaviorspace-run-number ".csv"))[
    file-open (word "FinalData-" behaviorspace-run-number ".csv")
    file-type "Run Number,"
    file-type "Tick, " 
    file-type "Population Size, "
    file-type "Current Prison Population, "
    file-type "Max. Agent ID, "
    file-type "Background Risk, "
    file-type "Outside Risk Multiplier, "
    file-type "Maximum Daily Risk, "
    file-type "Food Energy, "
    file-type "Latent Period, "
    file-type "Infectious Period, "
    file-type "Carrier Probability, "
    file-type "Mortality Probability, "
    file-type "First Case, "
    file-type "Susceptible, "
    file-type "Exposed, "
    file-type "Infectious, "
    file-type "Carriers, "
    file-type "Recovered, "
    file-type "Dead (Disease), "
    file-type "Dead (Starvation), "
    file-type "Total Dead, "
    file-print "R + C + D, "
  file-close]
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;       BASIC METHODS       ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


to go
  ask patches [
    if outside? and building-type = 4 [set pcolor 38] ; changes the color of ration area patches to distinguish them from other outside patches.
    if outside? and building-type = 5 [set pcolor 39]]; all non-ration-area outside patches are set to the background color.
  
  ask agents [                                                          
    set time-in-camp time-in-camp + 1                                ; each agent incrememnts the number of ticks it has been in the camp as its first action during a tick.
    if energy > 200 and disease-status != 5 [set needs-group? true]  ; agents who have the option to choose to move outside, based on dis. status and energy make themselves eligible to be incorporated into a group
    set gang-global 0
    set group-leader? false                                          ; the following two lines reset 
    set new-regiment-leader? false
    set adj-current-risk 0
    if time-infected != ticks + 1 [
      set newly-infected? false
    ] ; closes if time-infected
  ]
  
  ask turtles [
    if time-infected != ticks + 1 [
    set newly-dead? false]
  ] ; closes ask turtles
  
  ask one-of agents [set-timekeeper
    set latrine-risk determine-latrine-risk
    set outside-risk latrine-risk * outside-risk-multiplier]
    if timekeeper = 1 [
      if not ration-getters-chosen? [pick-ration-getter]
  ] 
  
  ask agents [
    if (timekeeper = 1 or timekeeper = 7) [set current-risk background-risk]
    if (disease-status = 1 or disease-status = 2)
      [update-disease-status]
    if timekeeper = 6 or timekeeper = 12 [
      set adj-current-risk (current-risk + random 3 - random 3) / max-daily-risk
      ifelse cumulative-risk < 100
       [set cumulative-risk cumulative-risk * (1 + adj-current-risk)]
       [set cumulative-risk 100]
    ]
    determine-if-infected cumulative-risk
    if disease-status != 5 and not starved? [
      check-movement-energy
    ] ; closes if dis-status !=5

    if timekeeper = 12 [set ration-getter? false]
        set food-risk-checked? false
        set step-completed? true

  ] ; closes ask agents
   
  if timekeeper = 12 [set ration-getters-chosen? false]
 
  set prison-pop count agents with [disease-status != 5 and not starved?]
 
  if ((timekeeper = 6 or timekeeper = 12) and prison-pop <= influx-cutoff) [
     let prob-create random-float 1.00
     if prob-create < prob-influx [
       ask agents [if shape = "square" [set shape "circle"]]
       set max-agent-id max [agent-id] of turtles
       set max-regiment-number max [regiment] of turtles
       create-new-reg-leaders
     ]
  ]
   
  set prison-pop count agents with [disease-status != 5 and not starved?]
  update-daily-output  
  if ticks + 1 = run-length [
    update-final-output
    stop
  ]
  
  tick
 
end

;------------------------------------------------------------------------

to set-timekeeper
  let counter ticks + 1
  if (remainder (counter - 1) 6 = 0) and (remainder (counter - 37) 42 != 0) [set timekeeper 1]
  if (remainder (counter - 2) 6 = 0) and (remainder (counter - 38) 42 != 0) [set timekeeper 2]
  if (remainder (counter - 3) 6 = 0) and (remainder (counter - 39) 42 != 0) [set timekeeper 3]
  if (remainder (counter - 4) 6 = 0) and (remainder (counter - 40) 42 != 0) [set timekeeper 4]
  if (remainder (counter - 5) 6 = 0) and (remainder (counter - 41) 42 != 0) [set timekeeper 5]
  if (remainder (counter - 6) 6 = 0) and (remainder (counter - 42) 42 != 0) [set timekeeper 6]

  if remainder (counter - 37) 42 = 0 [set timekeeper 7]
  if remainder (counter - 38) 42 = 0 [set timekeeper 8]
  if remainder (counter - 39) 42 = 0 [set timekeeper 9]
  if remainder (counter - 40) 42 = 0 [set timekeeper 10]
  if remainder (counter - 41) 42 = 0 [set timekeeper 11]
  if remainder (counter - 42) 42 = 0 [set timekeeper 12]
end

;------------------------------------------------------------------------

to initialize-home
    
  
  let home-patches (patch-set patches with [building-id = [barracks] of myself and not occupied?])
  ifelse any? home-patches 
    [let dest-patch one-of home-patches
     assign-location (dest-patch)
    ] ; closes if of any? home-patches
    [reassign-barracks]; close else of any? home-patches.  If there are no unoccupaied patches in the agent's assigned dwelling, the agent must find a new place to live.
  
end

;------------------------------------------------------------------------

to reassign-barracks 
  
   let open-home-patches (patch-set patches with [building-type = 1 and not occupied?]) 
     set barracks [building-id] of one-of open-home-patches
     let other-residents (turtle-set agents with [barracks = [barracks] of myself])
     let new-housemate one-of other-residents
     set barracks [barracks] of one-of new-housemate
    
  let new-home-patches (patch-set patches with [building-id = [barracks] of myself and not occupied?])
  let new-home one-of new-home-patches
  assign-location (new-home)
end

;------------------------------------------------------------------------


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Destination-Related Movement Methods;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to find-activities
  if (timekeeper = 1 or timekeeper = 2 or timekeeper = 3)  [do-MonSat-Day-Acts]
  if (timekeeper = 4 or timekeeper = 10) [do-Evening-Acts]
  if timekeeper = 7 [do-Sun-AM-Acts]
  if timekeeper = 8 [do-Sunday-Ration-Acts]
  if timekeeper = 9 [do-Sunday-PM-Acts]

end

;------------------------------------------------------------------------

to check-movement-energy
  let movement-chance random 10
  let todays-energy energy + random 50
  set todays-energy todays-energy - random 50
  if disease-status != 5 [ 
   ifelse todays-energy <= 0 
      [set starved? true
        set energy todays-energy
        type "Agent " type agent-id type " has starved to death. Tick = " print ticks
       create-ghost]
      [ifelse (timekeeper = 4 or timekeeper = 10) 
         [find-activities]
  [ifelse todays-energy <= 200
    [
      ifelse movement-chance < 3 
      [move-barracks (gang-size)
       let prob-latrine random-float 1.00
       let latrine-cutoff 0
       ifelse disease-status = 2 [set latrine-cutoff 0.2][set latrine-cutoff 0.05]
           if prob-latrine < latrine-cutoff  [ 
             set current-risk current-risk + latrine-risk
             set bathroom-tick ticks]]
      [do-nothing]
    ] ; closes if of "todays-energy <= 200"
    [
      ifelse todays-energy <= 400
            [ifelse movement-chance < 5
              [find-activities] 
              [move-barracks (gang-size)
               let prob-latrine random-float 1.00
               let latrine-cutoff 0
               ifelse disease-status = 2 [set latrine-cutoff 0.2][set latrine-cutoff 0.05]
                 if prob-latrine < latrine-cutoff  [ 
                   set current-risk current-risk + latrine-risk
                   set bathroom-tick ticks]]
    ] ; closes if of "todays-energy <= 400"
      [
        ifelse todays-energy <= 600
           [ifelse movement-chance < 8 
             [find-activities]
             [move-barracks (gang-size)
              let prob-latrine random-float 1.00
              let latrine-cutoff 0
              ifelse disease-status = 2 [set latrine-cutoff 0.2][set latrine-cutoff 0.05]
                if prob-latrine < latrine-cutoff  [ 
                  set current-risk current-risk + latrine-risk
                  set bathroom-tick ticks]]
          ] ; closes if of "todays-energy <= 600"
        [find-activities] ; else of "todays-energy <= 600"
      ] ; closes else of "todays-energy <= 400"
    ] ; closes else of "todays-energy <= 200"
     ] ; closes else of "timekeeper"
  if ((timekeeper = 3 or timekeeper = 9) and food-risk-checked? = false) [update-food-risk]
  ] ; closes else of "todays-energy <= 0"
  ] ; closes if disease-status != 5
end

;------------------------------------------------------------------------
    
to do-nothing
  let dest-patch nobody
  ifelse present-location = barracks 
    [
     set needs-group? false
     set group-leader? false
     set dest-patch patch-here
     set energy energy - 2
     set barracks-risk determine-barracks-risk
     set current-risk current-risk + barracks-risk
    ]
    [move-barracks (gang-size)]
end



;------------------------------------------------------------------------

to move-worship
  
  let dest-patch nobody
  let worship-patches (patch-set patches with [(pxcor >= 59 and pxcor <= 69) and (pycor >= 49 and pycor <= 59) and outside? and not occupied?])
    ask worship-patches [set pcolor 108]
  
  ifelse any? worship-patches 
    [set dest-patch one-of worship-patches with [occupied? = false]
     set energy energy - 6
     set current-risk current-risk + outside-risk]
    [set dest-patch patch-here                           ; if agents cannot find space in the 'church' area, they go back to their barracks because they are sad and alone.
     set energy energy - 3
     set barracks-risk determine-barracks-risk
     set current-risk current-risk + barracks-risk]
    
  assign-location (dest-patch)

end


;------------------------------------------------------------------------

to move-barracks [size-of-gang]
  
  let dest-patch nobody
  let home-patches (patch-set patches with [building-id = [barracks] of myself and not occupied?])
  ifelse count home-patches >= size-of-gang
    [set dest-patch one-of home-patches with [occupied? = false]
     set energy energy - 3
     set barracks-risk determine-barracks-risk
     set current-risk current-risk + barracks-risk
    ]
    [set dest-patch patch-here
      ifelse outside? 
      [ ; if outside
        set energy energy - 6
        set current-risk current-risk + outside-risk
      ]
      [ ;else outside
        set energy energy - 3
        set current-risk current-risk + barracks-risk
      ]
    ]
  assign-location (dest-patch)

  ;if barracks = 101 [type "(move-barracks) Agent " type agent-id type " has a new current risk of " print current-risk]
end

;------------------------------------------------------------------------

to move-outside [size-of-gang id-of-gang gang-members]
  
  let dest-patch nobody
  let outside-open-patches (patch-set patches with [outside? and not occupied?])
  let adequate-radius 1
    if size-of-gang > 5 and size-of-gang <= 12 [set adequate-radius 2]
    if size-of-gang > 12 [set adequate-radius 3]
  set dest-patch one-of outside-open-patches with [count outside-open-patches in-radius adequate-radius >= size-of-gang]
  let gang-patches no-patches
 
  if group-leader? [  
   ifelse dest-patch != nobody [
      assign-location (dest-patch)
      set energy energy - 6
      set current-risk current-risk + outside-risk
           if agent-id = 1 or agent-id = 2 or agent-id = 3 []
      ask dest-patch [
        set occupied? true
        set gang-patches (patch-set patches in-radius adequate-radius with [outside? and occupied? = false])
        ask gang-patches [set pcolor 106] 
       ] ; closes ask dest-patch
        
      ask gang-members with [group-leader? = false and disease-status != 5][
        if any? gang-patches with [occupied? = false][
          set dest-patch one-of gang-patches with [occupied? = false]
          assign-location (dest-patch)
          set energy energy - 6
          set current-risk current-risk + outside-risk
          ask dest-patch [set occupied? true]
        ]
      ] ; closes ask gang-members
    ] ; closes if of ifelse any? open patches
    [
      ask gang-members with [disease-status != 5][
       set dest-patch patch-here                              ; This block is designed for agents who want to move outside but cannot find enough contiguous patches to move as a group.  They adjust their energy as if
       set energy energy - 2                                  ; they did not move (see procedure do-nothing), and their risk appropriatly for their location before the move attempt
       ifelse [building-type] of patch-here = 1             
         [ set barracks-risk determine-barracks-risk
           set current-risk current-risk + barracks-risk]
         [set current-risk current-risk + outside-risk] 
      ]; closes ask gang-members
    ]; else of ifelse any? open-patches
  ]; closes if group-leader?

end

;------------------------------------------------------------------------

;to move-hospital
  
;  let dest-patch nobody
;  let hospital-patches (patch-set patches with [building-type = 2 and not occupied?]) 
;  ifelse any? hospital-patches 
;    [set dest-patch one-of hospital-patches with [occupied? = false]]
;    [set dest-patch patch-here]
;    
;  assign-location (dest-patch)
;end

;------------------------------------------------------------------------

to get-rations
  
  let dest-patch nobody
  set needs-group? false
  
  ;type agent-id type " is currently at patch " print patch-here
  let ration-patches (patch-set patches with [building-type = 4 and not occupied?]) 
 ; print ration-patches
  ifelse any? ration-patches 
    [set dest-patch one-of ration-patches with [occupied? = false]
      ;type agent-id type " has moved to patch " print patch-here
      ]
    [set dest-patch patch-here]
    
  assign-location (dest-patch)
  set energy energy - 6
  set current-risk current-risk + outside-risk

end




;;;;;;;;;;;;;;;;;;;;;;;
;;Movement Submethods;;
;;;;;;;;;;;;;;;;;;;;;;;


to assign-location [destination]
   ask patch-here [set occupied? false]
   ifelse destination = nobody
     [print "ERROR IN ASSIGN-LOCATION. Nobody returned as destination"]
     [setxy [pxcor] of destination [pycor] of destination]
   set present-location [building-id] of patch-here
   ask patch-here [set occupied? true]
end


;------------------------------------------------------------------------

;------------------------------------------------------------------------

to-report make-regiment-group
  
  let regiment-set no-turtles
  let regiment-total-set no-turtles
  set gang-global gang-global + 1
  set gang-id gang-global
  
  set group-leader? true
  set gang-size 1
  set needs-group? false
 
  set regiment-set (turtle-set regiment-set self)
  ;type "agent " type agent-id type " is group leader of gang# " type gang-id type " and needs-group? " print needs-group?
    

  set regiment-total-set (turtle-set regiment-total-set agents with [barracks = [barracks] of myself and regiment = [regiment] of myself and needs-group?])
  if any? regiment-total-set [
    ask regiment-total-set [
      if gang-size < 6 [
        set regiment-set (turtle-set regiment-set self)
        set gang-id [gang-id] of myself
        set gang-size gang-size + 1
        set needs-group? false
        ;type "agent " type agent-id type " is in gang# " type gang-id type " with " type gang-size print " people in the group."
      ]
    ]
  ]

  report regiment-set
  
end

;-----------------------------------------------------------------------

to pick-ration-getter
  
  ask one-of agents with [barracks = 101 and ration-getter? = false] 
    [set ration-getter? true]
  ask one-of agents with [barracks = 101 and ration-getter? = false] 
    [set ration-getter? true]

  ask one-of agents with [barracks = 102 and ration-getter? = false] 
    [set ration-getter? true]
  ask one-of agents with [barracks = 102 and ration-getter? = false] 
    [set ration-getter? true]
    
  ask one-of agents with [barracks = 103 and ration-getter? = false] 
    [set ration-getter? true]
  ask one-of agents with [barracks = 103 and ration-getter? = false] 
    [set ration-getter? true]
    
  ask one-of agents with [barracks = 104 and ration-getter? = false] 
    [set ration-getter? true]
  ask one-of agents with [barracks = 104 and ration-getter? = false] 
    [set ration-getter? true]
    
  ask one-of agents with [barracks = 105 and ration-getter? = false] 
    [set ration-getter? true]
  ask one-of agents with [barracks = 105 and ration-getter? = false] 
    [set ration-getter? true]
    
  ask one-of agents with [barracks = 106 and ration-getter? = false] 
    [set ration-getter? true]
  ask one-of agents with [barracks = 106 and ration-getter? = false] 
    [set ration-getter? true]

  set ration-getters-chosen? true
  
end




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;      DAILY METHODS      ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;




to do-MonSat-Day-Acts
  
  let prob-outside random-float 1.0 
  let regiment-gang no-turtles
  
 let prob-latrine random-float 1.00
 let latrine-cutoff 0
 ifelse disease-status = 2 [set latrine-cutoff 0.2][set latrine-cutoff 0.05]
     if prob-latrine < latrine-cutoff  [ 
       set current-risk current-risk + latrine-risk
       set bathroom-tick ticks]
  
  if needs-group? [
    set regiment-gang make-regiment-group]
      ;if gang-id = 1 [set color blue + 3]
      ;if gang-id = 2 [set color yellow]
      ;if gang-id = 3 [set color green]
  if group-leader? [
   ifelse prob-outside < 0.8 ; - calorie deficiency proportion          
     [move-outside gang-size gang-id regiment-gang]
     [ask regiment-gang [move-barracks (gang-size)]]
  ]
end


;------------------------------------------------------------------------

to do-Evening-Acts
  set gang-size 1
  let prob-latrine random-float 1.00
  let latrine-cutoff 0
  ifelse disease-status = 2 [set latrine-cutoff 0.2][set latrine-cutoff 0.05]
     if prob-latrine < latrine-cutoff  [ 
       set current-risk current-risk + latrine-risk
       set bathroom-tick ticks]                                                            ; Asymptomatic agent sare equally likely to use the latrine as uninfected individuals, though they can/will transmit disease 
                                                                                           ; at a low rate.      
 
  move-barracks (gang-size)
  set current-risk current-risk + determine-food-risk
  ;if barracks = 101 [type "Agent " type agent-id type " in 101 has a food-risk of " print current-risk
  set energy energy + food-energy + random 2                                      ;; Agents will be gaining a net of 9-11 energy when they eat their rations in the evening.  The base energy intake of 14 here                                                                      ;; is intended to offset the 5 energy used to move in the barracks during the evening acts step.
end 

;------------------------------------------------------------------------

to do-Sun-AM-Acts  ;This method is the same as the do-MonSat-Day-Acts method at the moment, but is separated out so that informal church activities can be added later.
  
  let prob-outside random-float 1.0
  let prob-worship random-float 1.0 

  let regiment-gang no-turtles
 
  let prob-latrine random-float 1.00
  let latrine-cutoff 0
  ifelse disease-status = 2 [set latrine-cutoff 0.2][set latrine-cutoff 0.05]
     if prob-latrine < latrine-cutoff  [ 
       set current-risk current-risk + latrine-risk
       set bathroom-tick ticks]
  
  if prob-worship < 0.2 [
    move-worship
    set needs-group? false
  ]
  
  if needs-group? [set regiment-gang make-regiment-group] 
  if group-leader? [
    ifelse prob-outside < 0.8 ; - calorie deficiency proportion           ;; As calorie deficiency increases, an agent's probability of moving from their barracks decreases accordingly.
      [move-outside gang-size gang-id regiment-gang]
      [ask regiment-gang [move-barracks (gang-size)]]
  ]
end
;------------------------------------------------------------------------


to do-Sunday-Ration-Acts
  
  let prob-outside random-float 1.0 
  let regiment-gang no-turtles
  set gang-size 1  
  
  let prob-latrine random-float 1.00
  let latrine-cutoff 0
  ifelse disease-status = 2 [set latrine-cutoff 0.2][set latrine-cutoff 0.05]
     if prob-latrine < latrine-cutoff  [ 
       set current-risk current-risk + latrine-risk
       set bathroom-tick ticks]
  
  if ration-getter? = true [
    get-rations
    set needs-group? false
  ]

  if needs-group? [set regiment-gang make-regiment-group] 
  if group-leader? [
    ifelse prob-outside < 0.8 ; - calorie deficiency proportion           ;; As calorie deficiency increases, an agent's probability of moving from their barracks decreases accordingly.
      [move-outside gang-size gang-id regiment-gang]
      [ask regiment-gang [move-barracks (gang-size)]]
  ]
end

;------------------------------------------------------------------------

to do-Sunday-PM-Acts
  
  let prob-outside random-float 1.0 
  let regiment-gang no-turtles
  
  let prob-latrine random-float 1.00
  let latrine-cutoff 0
  ifelse disease-status = 2 [set latrine-cutoff 0.2][set latrine-cutoff 0.05]
     if prob-latrine < latrine-cutoff  [ 
       set current-risk current-risk + latrine-risk
       set bathroom-tick ticks]
  
  if ration-getter? = true [
    set gang-size 1
    move-barracks (gang-size)
  ]
  
  if needs-group? [
   set regiment-gang make-regiment-group]
       ;if gang-id = 1 [set color blue + 3]
       ;if gang-id = 2 [set color yellow]
       ;if gang-id = 3 [set color green]
 if group-leader? [
   ifelse prob-outside < 0.8 ; - calorie deficiency proportion           ;; As calorie deficiency increases, an agent's probability of moving from their barracks decreases accordingly.
      [move-outside gang-size gang-id regiment-gang]
      [ask regiment-gang [move-barracks (gang-size)]]
  ] 

end


 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;     DISEASE METHODS     ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to update-disease-status
  ; An agent method called near the beginning of the go method. Depending on their current disease status and the value of relevant timing variables,
  ; agents will transition to the next disease status or reduce the time remaining for the current status (or die, with some probability, if they are infectious). The durations of disease
  ; stages are equal for all agents.
  
  ; Susceptible agents do nothing
  
  let carrier-threshold 0
  
  ; Exposed agents must check the value of time-to-infectious to see if they should become infectious this time period:
  if disease-status = 1 [
    ifelse time-to-infectious = 0
       [set disease-status 2
        set time-to-recovery infectious-period 
        set color red]
       [set time-to-infectious time-to-infectious - 1]
    ]
  
  ; Infectious agents first check whether they will die this time period. If they survive, they must check the value of time-to-recovery to see if they should recover this time period. 
  if disease-status = 2 [
    let death-threshold random-float 1.0
    if death-threshold < death-prob 
      [create-ghost]  
    ifelse time-to-recovery = 0 and disease-status = 2 ; The ifelse statement includes the disease-status component so agents who die this tick don't override changes made in kill-node
    [
      ifelse energy <= 400
        [set carrier-threshold random-float 0.80] ; agents are more likely to become carriers if they are malnourished.
        [set carrier-threshold random-float 1.00]
      ifelse carrier-threshold < carrier-prob [
        set disease-status 3  ; these are carriers, who can continue in this state for the rest of the model or until their deaths (starvation)
        set color 134]
       [set disease-status 4  ; these are recovered individuals.  We assume that only one strain is circulating, such that recovery confers permanent immunity (See Todar's bible of bacteria).
        set color sky]
    ]
       [set time-to-recovery time-to-recovery - 1]
  ]
   
  
  ; Recovered agents do nothing.
end

;------------------------------------------------------------------------

to determine-if-infected [cum-risk]
  ; this method establishes an agent's probability of becoming infected (changing to dis-status 2) based on their cumulative-risk at any given timestep.  
  
  let inf-prob 0
  let infection-threshold random-float 1.00
  ifelse cum-risk < 80 
    [set inf-prob cum-risk * 0.0005]  ; The multiplier here will likely need to be altered later in such a way that the prevalence of the disease during simulations approximates what is known from the literature. 
                                     ; This is done by finding the slope of the line when cumulative risk is plotted against infection probability.  The 0.8 in the 'else' will also need to be adjusted.
    [set inf-prob 0.0005]
  if infection-threshold < inf-prob and disease-status = 0 [
    set disease-status 1
    set color orange
    set time-to-infectious latent-period - 1
    set time-infected ticks + 1
    ifelse outside?
      [set place-infected 999]
      [set place-infected [building-id] of patch-here]
    set newly-infected? true
  ]
end

;------------------------------------------------------------------------

to create-ghost
  if not starved? 
  [set disease-status 5
   ;type "Agent " type agent-id type " has died from dysentery on the way to Oregon. Tick = " print ticks
   ]
  set newly-dead? true
  set needs-group? false
  set time-died ticks + 1 ; as in the transmission methods above, the plus one needs to correct for how the go method keeps track of time
  ifelse outside?
  [set place-died 999]
  [set place-died [building-id] of patch-here]
  
  if ration-getter? 
  [
    ifelse any? agents with [barracks = [barracks] of myself and ration-getter? = false]
     [ask one-of agents with [barracks = [barracks] of myself and ration-getter? = false] 
    [set ration-getter? true]
     ] ; closes if of ifelse any?
    [type "No replacement ration-getter is available for barracks " print [barracks] of myself]
  ] ; closes if ration-getter?
  
  hatch-ghosts 1 [
  set shape "ghost"
  set color white
  set size 2
  ask patch-here
  [set occupied? false]
  hide-turtle  
  ]
  
  ; The following statements set the size of ghosts in the cemetery to be proportional to the number of ghosts in order to provide a
  ; visualization of the number of deaths during the epidemic. At present this code results in multiple visible ghosts of different 
  ; sizes superimposed on one another at the location of the cemetery. The icons are also centered over the patch (1,1), with the 
  ; result that when the ghost gets large enough, only part of it can be seen. These are commented out for batch runs, but need to 
  ; be uncommented for gui runs.
  setxy 1 1
  ;  let prop-ghosts (count ghosts / pop-size)
  ;  if prop-ghosts > 0.01  [set size floor (prop-ghosts * 50)]
end

;------------------------------------------------------------------------

to update-food-risk
  
  ; This method takes into account an increasing probability of contamination as the cooks' risks increase.  Most recent studies of dysentery outbreaks suggest that food contamination is a consequence of
  ; cooks' activities (i.e., not washing hands), and so we are assuming that the risk associated with passive environmental contamination of food is negligible.
  let total-food-risk 0
  
  set food-risk-checked? true
  let cooks-risk1 0
  let cooks-risk2 0
  let average-cooks-risk 0
  let cook-1-set? false
  
  ;ask agents with [barracks = [barracks] of myself and ration-getter? = true] [
  ;  type "agent " type agent-id type " is a cook in barracks " print barracks
  ;]
    
  ask agents with [barracks = [barracks] of myself] [ 
    
    set food-risk-checked? true

    ifelse ration-getter? = true and cook-1-set? = false
       [set cooks-risk1 cumulative-risk
        ; type "agent " type agent-id type " is cook 1. His risk is " print cooks-risk1
         set cook-1-set? true]
       [if ration-getter? = true and  cook-1-set? = true
         [set cooks-risk2 cumulative-risk
          ;type "agent " type agent-id type " is cook 2. His risk is " print cooks-risk2
        ]
     ]
  ]
  
  set average-cooks-risk ((cooks-risk1 + cooks-risk2) / 2)
 ; type "The average cooks risk for " type barracks type " is " print average-cooks-risk
  set total-food-risk total-food-risk + average-cooks-risk
  
  let food-array-row find-barracks-row
  
 ; type "Agent " type agent-id type " is assigned to barracks " type barracks type ", find-barracks-row returned " print food-array-row 
 ; type "The total food risk for barracks " type barracks type " is " print total-food-risk
  
  matrix:set barracks-food-array food-array-row 1 total-food-risk
;  print risk-array
end

;------------------------------------------------------------------------

to-report determine-food-risk
  
  let food-array-row find-barracks-row
  let food-risk 0
  
  ;if barracks = 101 [type "Agent " type agent-id type " in 101 has a pre-check risk of " print current-risk]
  set food-risk matrix:get barracks-food-array food-array-row 1
  ;if barracks = 101 [type "Agent " type agent-id type " has a food-risk of " print food-risk]
  
  report food-risk
end

;------------------------------------------------------------------------

to-report determine-latrine-risk
  if any? agents with [bathroom-tick >= 1]
  [
  let prop-inf 0
  set latrine-risk 0
  let tick-now ticks + 1
  let count-inf count agents with [(disease-status = 2 or disease-status = 3) and (bathroom-tick > (tick-now - 7))]
  let recent-bathroom-count count agents with [bathroom-tick > (tick-now - 7)]
  set prop-inf count-inf / recent-bathroom-count
  
  
  if prop-inf <= 0.25 [set latrine-risk 20 * prop-inf]
  if prop-inf > 0.25 and prop-inf <= 0.75 [set latrine-risk ((70 * prop-inf) - 12.5)]
  if prop-inf > 0.75 [set latrine-risk 40]

  ]
  report latrine-risk

end

;------------------------------------------------------------------------

to-report determine-barracks-risk
  
  let prop-inf 0
  set barracks-risk 0
  set prop-inf count agents with [(disease-status = 2 or disease-status = 3) and barracks = [barracks] of myself] / (count agents with [barracks = [barracks] of myself])
   
  if prop-inf <= 0.25 [set barracks-risk 4 * prop-inf]
  if prop-inf > 0.25 and prop-inf <= 0.75 [set barracks-risk ((14 * prop-inf) - 2.5)]
  if prop-inf > 0.75 [set barracks-risk 8]
  
  ;if barracks = 101 [type "Agent " type agent-id type " has a current barracks-risk of " print barracks-risk]
  report barracks-risk
  
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MISCELLANEOUS METHODS      ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to-report find-barracks-row
  let bldgcount 1
  let barracks-row 0
  while [barracks-row = 0 and bldgcount < num-barracks] [
  if item bldgcount building-list = barracks
     [set barracks-row bldgcount]
     set bldgcount bldgcount + 1
  ]
  report barracks-row
end

;------------------------------------------------------------------------


;------------------------------------------------------------------------

to create-new-reg-leaders

 ; figure out how many open spaces exist in each barrack.  New prisoners with the same regiment-id should be preferrentially assigned to the same barracks.
  
 create-agents num-new-reg-leaders + random 3 [
   set agent-id max-agent-id + 1
   set max-agent-id max-agent-id + 1
   set barracks 0
   set regiment max-regiment-number + 1
   set max-regiment-number max-regiment-number + 1
   set age 21 + random 4 - random 4
   set energy 1500
   set starved? false
   set time-in-camp 1
   let dest-patch one-of patches with [outside? and not occupied?]
   assign-location (dest-patch)
   
   set time-to-infectious latent-period
   set time-to-recovery infectious-period
   set time-infected -1
   set place-infected -1
   set time-died -1
   set place-died -1
   
   set ration-getter? false
   set new-regiment-leader? true
   set current-risk background-risk
   set cumulative-risk background-risk
   set barracks-risk 0
   set food-risk-checked? false
      
   set newly-infected? false
   set newly-dead? false
   set step-completed? false
      
   set shape "square"
   set color lime
   set size .8
   
  ; type "Agent " type agent-id type " has been spawned.  Regiment = " type regiment type " Tick = " print ticks  
   
   set gang-id 0
 ] 
 assign-regiment-and-barracks
end

;------------------------------------------------------------------------
 
to assign-regiment-and-barracks  
  ask agents with [shape = "square" and new-regiment-leader?] [
   
    let num-new-regiment-mates random 3
    let new-regiment-size num-new-regiment-mates + 1
    let searchcount 0  
    let available-beds 0
    set barracks 101 + random (num-barracks)
    set available-beds count patches with [building-id = [barracks] of myself and (occupied? = false)]
    
    while [(searchcount <= num-barracks * 5) and (new-regiment-size > available-beds)] 
       [
        set barracks 101 + random (num-barracks)
        set available-beds count patches with [building-id = [barracks] of myself and (occupied? = false)]
        set searchcount searchcount + 1
       ]
    ifelse (searchcount <= num-barracks * 5)
      [
        move-barracks (1)
        ;type "Agent " type agent-id type " has hatched " type num-new-regiment-mates type " MINIONS, and has chosen barracks = " print barracks
        hatch-agents num-new-regiment-mates 
        [
          set agent-id max-agent-id + 1
          set max-agent-id max-agent-id + 1
          set age 21 + random 4 - random 4
          set new-regiment-leader? false
 ;         type "Agent " type agent-id type " has been hatched.  Regiment = " type regiment type " Tick = " type ticks type " Barracks = " print barracks
          move-barracks (1)
        ] ; closes hatched agent features
    ] ; closes if count patches
      
    [ ; opens else count-patches.  Agents enter this loop if they cannot find enough space for all of the regiment members to live together.
  ;    type "Agent " type agent-id type " has hatched " type num-new-regiment-mates type " MINIONS, and has chosen barracks = " print barracks
      hatch-agents num-new-regiment-mates 
      [
        set agent-id max-agent-id + 1
        set max-agent-id max-agent-id + 1
        set age 21 + random 4 - random 4
        set new-regiment-leader? false
   ;     type "Agent " type agent-id type " has been hatched.  Regiment = " type regiment type " Tick = " type ticks type " DIFFERENT Barracks = " print barracks
      ]
      ask agents with [regiment = [regiment] of myself] [
        while [(searchcount <= num-barracks * 5) and (available-beds = 0)]
        [
          set barracks 101 + random (num-barracks)
          set available-beds count patches with [building-id = [barracks] of myself and (occupied? = false)]
          set searchcount searchcount + 1
        ]
        if available-beds = 0 [set barracks 500]
        move-barracks (1)
      ] ; closes ask agents with regiment of myself
    ] ; closes else count-patches
  ] ; closes agents with shape = square and regiment-leader
    
end

;------------------------------------------------------------------------



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;Data Collection and Display Update Methods;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to update-daily-output
  tally
  draw-plots
  write-to-daily-file
end

;------------------------------------------------------------------------

to tally
  set num-susceptible count agents with [disease-status = 0]
  set num-exposed count agents with [disease-status = 1] 
  set num-infectious count agents with [disease-status = 2]
  set num-carrier count agents with [disease-status = 3]
  set num-recovered count agents with [disease-status = 4]
  set num-dis-dead count agents with [disease-status = 5]
  set num-starved count agents with [starved? = true]
  set num-dead count ghosts                               ; should equal num-dis-dead + num-starved.
 
  set RCD num-recovered + num-carrier + num-dis-dead ;size of epidemic 

end

;------------------------------------------------------------------------

to draw-plots
  set-current-plot "Course of Epidemic"
  set-current-plot-pen "Susceptible Agents"
  plot num-susceptible
  set-current-plot-pen "Exposed Agents"
  plot num-exposed
  set-current-plot-pen "Infectious Agents"
  plot num-infectious
  set-current-plot-pen "Asymptomatic Carriers"
  plot num-carrier
  set-current-plot-pen "Recovered Agents"
  plot num-recovered
  
  set-current-plot "Distribution of Deaths"
  set-current-plot-pen "Total Dead"
  plot num-dead
  set-current-plot-pen "Dead from Disease"
  plot num-dis-dead
  set-current-plot-pen "Dead from Starvation"
  plot num-starved
  
  
end

;------------------------------------------------------------------------

to write-to-daily-file
  file-open (word "DailyData-" behaviorspace-run-number ".csv")
  file-type (word behaviorspace-run-number ", ")
  file-type (word (ticks + 1) ", ") ; Unlike the Repast model, the Netlogo model clock starts at tick 0. One tick is added in data recording to ensure consistency with the Repast model. 
  ; For example, the first tick the first case is infectious is latent period + 1, i.e. tick 7 if the latent period is 6 days. This also ensures that the data recording is consistent
  ; with the visualization.
  file-type (word init-pop-size ", ")
  file-type (word prison-pop ", ")
  file-type (word background-risk ", ")
  file-type (word outside-risk-multiplier ", ")
  file-type (word max-daily-risk ", ")
  file-type (word food-energy ", ")
  file-type (word latent-period ", ")
  file-type (word infectious-period ", ")
  file-type (word carrier-prob ", ")
  file-type (word death-prob ", ")
  file-type (word first-case ", ")
  file-type (word num-susceptible ", ")
  file-type (word count agents with [newly-infected?] ", ")
  file-type (word num-exposed ", ")
  file-type (word num-infectious ", ")
  file-type (word num-carrier ", ")
  file-type (word num-recovered ", ")
  file-type (word count ghosts with [newly-dead? and not starved?] ", ")
  file-type (word count ghosts with [newly-dead? and starved?] ", ")
  file-print (word num-dead ", ")
  file-close
end

;------------------------------------------------------------------------

to update-final-output
  write-to-cases-file
  write-to-final-file
end

;------------------------------------------------------------------------

to write-to-cases-file
  file-open (word "CasesData-" behaviorspace-run-number ".csv")
  ask agents [ ; JD NOTE: SEE NETWORK MODEL "FOREACH"
  file-type (word behaviorspace-run-number ", ")
  file-type (word (ticks + 1) ", ") ; See comment above in the write-to-daily-file method.
  file-type (word init-pop-size ", ")
  file-type (word prison-pop ", ")
  file-type (word background-risk ", ")
  file-type (word outside-risk-multiplier ", ")
  file-type (word max-daily-risk ", ")
  file-type (word food-energy ", ")
  file-type (word latent-period ", ")
  file-type (word infectious-period ", ")
  file-type (word carrier-prob ", ")
  file-type (word death-prob ", ")
  file-type (word first-case ", ")
  file-type (word first-case-barracks ", ")
  file-type (word first-case-regiment ", ")
  file-type (word agent-id ", ")
  file-type (word barracks ", ")
  file-type (word regiment ", ")
  file-type (word time-infected ", ")
  file-type (word place-infected ", ")
  file-type (word disease-status ", ")
  file-type (word time-died ", ")
  file-type (word place-died ", ")
  file-type (word starved? ", ")
  file-print (word time-in-camp ", ")
  ]
  file-close
end

;------------------------------------------------------------------------

to write-to-final-file
  file-open (word "FinalData-" behaviorspace-run-number ".csv")
  file-type (word behaviorspace-run-number ", ")
  file-type (word (ticks + 1) ", ") ; See comment above in the write-to-daily-file method.
  file-type (word init-pop-size ", ")
  file-type (word prison-pop ", ")
  file-type (word background-risk ", ")
  file-type (word outside-risk-multiplier ", ")
  file-type (word max-daily-risk ", ")
  file-type (word food-energy ", ")
  file-type (word latent-period ", ")
  file-type (word infectious-period ", ")
  file-type (word carrier-prob ", ")
  file-type (word death-prob ", ")
  file-type (word first-case ", ")
  file-type (word num-susceptible ", ")
  file-type (word num-exposed ", ")
  file-type (word num-infectious ", ")
  file-type (word num-carrier ", ")
  file-type (word num-recovered ", ")
  file-type (word num-dis-dead ", " )
  file-type (word num-starved ", ")
  file-type (word num-dead ", ")
  file-print (word RCD ", ")
  file-close
end
@#$#@#$#@
GRAPHICS-WINDOW
213
10
598
416
-1
-1
5.0
1
10
1
1
1
0
0
0
1
0
74
0
74
1
1
1
ticks
30.0

BUTTON
25
27
89
60
Setup
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
105
28
168
61
Step
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
105
72
168
105
Run
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
18
380
190
413
death-prob
death-prob
0
1
0.015
.001
1
NIL
HORIZONTAL

SLIDER
18
244
190
277
latent-period
latent-period
0
36
18
6
1
ticks
HORIZONTAL

SLIDER
18
286
190
319
infectious-period
infectious-period
0
60
42
6
1
ticks
HORIZONTAL

PLOT
992
11
1486
340
Course of Epidemic
tick
number of agents
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"Susceptible Agents" 1.0 0 -16777216 true "" ""
"Exposed Agents" 1.0 0 -955883 true "" ""
"Infectious Agents" 1.0 0 -2674135 true "" ""
"Recovered Agents" 1.0 0 -13791810 true "" ""
"Asymptomatic Carriers" 1.0 0 -2064490 true "" ""

SLIDER
19
112
192
145
run-length
run-length
0
1000
850
1
1
NIL
HORIZONTAL

BUTTON
25
71
92
105
NIL
profile
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
793
12
967
45
background-risk
background-risk
0
10
1
01
1
NIL
HORIZONTAL

SLIDER
610
12
782
45
carrier-prob
carrier-prob
0
0.1
0.05
0.005
1
NIL
HORIZONTAL

SLIDER
795
57
967
90
outside-risk-multiplier
outside-risk-multiplier
0
1
0.1
0.01
1
NIL
HORIZONTAL

SLIDER
19
336
191
369
food-energy
food-energy
0
25
10
1
1
NIL
HORIZONTAL

SLIDER
796
106
968
139
max-daily-risk
max-daily-risk
0
500
200
10
1
NIL
HORIZONTAL

PLOT
991
351
1487
611
Distribution of Deaths
tick
number of agents
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"Total Dead" 1.0 0 -16777216 true "" ""
"Dead from Disease" 1.0 0 -2674135 true "" ""
"Dead from Starvation" 1.0 0 -13791810 true "" ""

PLOT
599
161
988
494
Prison Population
tick
number of agents
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot prison-pop"

SLIDER
19
427
191
460
influx-cutoff
influx-cutoff
450
600
400
5
1
NIL
HORIZONTAL

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

ghost
false
0
Polygon -7500403 true true 30 165 13 164 -2 149 0 135 -2 119 0 105 15 75 30 75 58 104 43 119 43 134 58 134 73 134 88 104 73 44 78 14 103 -1 193 -1 223 29 208 89 208 119 238 134 253 119 240 105 238 89 240 75 255 60 270 60 283 74 300 90 298 104 298 119 300 135 285 135 285 150 268 164 238 179 208 164 208 194 238 209 253 224 268 239 268 269 238 299 178 299 148 284 103 269 58 284 43 299 58 269 103 254 148 254 193 254 163 239 118 209 88 179 73 179 58 164
Line -16777216 false 189 253 215 253
Circle -16777216 true false 102 30 30
Polygon -16777216 true false 165 105 135 105 120 120 105 105 135 75 165 75 195 105 180 120
Circle -16777216 true false 160 30 30

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.2.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="experiment" repetitions="100" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>count agents with [disease-status = 1]</metric>
    <metric>count agents with [disease-status = 2]</metric>
    <metric>count agents with [disease-status = 3]</metric>
    <metric>count agents with [disease-status = 4]</metric>
    <metric>count agents with [disease-status = 5]</metric>
    <enumeratedValueSet variable="run-length">
      <value value="850"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="latent-period">
      <value value="18"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="infectious-period">
      <value value="42"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-daily-risk">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="death-prob">
      <value value="0.015"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="carrier-prob">
      <value value="0.035"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="food-energy">
      <value value="18"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="outside-risk-multiplier">
      <value value="0.1"/>
    </enumeratedValueSet>
    <steppedValueSet variable="influx-cutoff" first="450" step="20" last="550"/>
  </experiment>
  <experiment name="Carrier Probability" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>count agents with [disease-status = 0]</metric>
    <metric>count agents with [disease-status = 1]</metric>
    <metric>count agents with [disease-status = 2]</metric>
    <metric>count agents with [disease-status = 3]</metric>
    <metric>count agents with [disease-status = 4]</metric>
    <metric>count agents with [disease-status = 5]</metric>
    <enumeratedValueSet variable="food-energy">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-daily-risk">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="background-risk">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influx-cutoff">
      <value value="550"/>
    </enumeratedValueSet>
    <steppedValueSet variable="carrier-prob" first="0.01" step="0.0050" last="0.05"/>
    <enumeratedValueSet variable="infectious-period">
      <value value="42"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="death-prob">
      <value value="0.015"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="latent-period">
      <value value="18"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="outside-risk-multiplier">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="run-length">
      <value value="850"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Latent Period" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>count agents with [disease-status = 0]</metric>
    <metric>count agents with [disease-status = 1]</metric>
    <metric>count agents with [disease-status = 2]</metric>
    <metric>count agents with [disease-status = 3]</metric>
    <metric>count agents with [disease-status = 4]</metric>
    <metric>count agents with [disease-status = 5]</metric>
    <enumeratedValueSet variable="carrier-prob">
      <value value="0.05"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="influx-cutoff">
      <value value="550"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="outside-risk-multiplier">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="run-length">
      <value value="850"/>
    </enumeratedValueSet>
    <steppedValueSet variable="latent-period" first="15" step="1" last="21"/>
    <enumeratedValueSet variable="infectious-period">
      <value value="42"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="food-energy">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-daily-risk">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="death-prob">
      <value value="0.015"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="background-risk">
      <value value="1"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="influx400" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>count agents with [disease-status = 0]</metric>
    <metric>count agents with [disease-status = 1]</metric>
    <metric>count agents with [disease-status = 2]</metric>
    <metric>count agents with [disease-status = 3]</metric>
    <metric>count agents with [disease-status = 4]</metric>
    <metric>count agents with [disease-status = 5]</metric>
    <enumeratedValueSet variable="influx-cutoff">
      <value value="400"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="run-length">
      <value value="850"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="carrier-prob">
      <value value="0.05"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="food-energy">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="infectious-period">
      <value value="42"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-daily-risk">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="outside-risk-multiplier">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="death-prob">
      <value value="0.015"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="background-risk">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="latent-period">
      <value value="18"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
0
@#$#@#$#@
